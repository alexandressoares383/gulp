const { src, dest, parallel } = require('gulp');
const less = require('gulp-less');
const minifyCss = require('gulp-csso');

// Compilando CSS
function compileCss() {
    return src('./dev/styles/less/*.less')
        .pipe(less())
        .pipe(minifyCss())
        .pipe(dest('./dist/css'))
}

function compileJs() {
    return src('./dev/styles/scripts/*.js')
        .pipe(dest('./dist/js'))
} 

exports.compileCss = compileCss;
exports.compileJs = compileJs;
exports.default = parallel(compileCss, compileJs);
